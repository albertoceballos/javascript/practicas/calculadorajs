var arrayOperaciones=[];
var total = 0;
window.addEventListener("load", function() {
    var numeros = document.querySelectorAll(".numero");
    for (var i = 0; i < numeros.length; i++) {
        numeros[i].addEventListener("click", clickNumeros);
    }
    var operadores=document.querySelectorAll(".operadores");
    for(var i=0;i<operadores.length;i++){
    	operadores[i].addEventListener("click",clickOperadores)
    }

    document.querySelector(".borrar").addEventListener("click",borrar);
    document.querySelector(".igual").addEventListener("click",clickIgual);
    document.querySelector(".borrarTodo").addEventListener("click",borrarTodo);
    document.querySelector(".cambiar").addEventListener("click",cambiar);

    //función para operar con el teclado:
 	window.addEventListener("keydown",(event)=>{
  		codigo=event.keyCode;
  		console.log(event.keyCode)
  		if((codigo>47 && codigo<58) || codigo==106 || codigo==107 || codigo==109 || codigo==111){
  			document.querySelector(".pantalla2").value+=event.key;
  		}
  	});
});

function clickNumeros(event) {
    var textoBoton = event.target.innerHTML;
    if(document.querySelector(".pantalla2").value==0){
    	document.querySelector(".pantalla2").value=textoBoton;
    }else{
    	document.querySelector(".pantalla2").value +=textoBoton;
    }
}

function clickOperadores(event) {
	var num=parseInt(document.querySelector(".pantalla2").value);
	console.log(isNaN(num));
	if(isNaN(num)==false){
		arrayOperaciones.push(num);
		document.querySelector(".pantalla2").value="";
		operacion=event.target.innerHTML;
		arrayOperaciones.push(operacion);
		document.querySelector(".pantalla").value="";
		for(var i=0;i<arrayOperaciones.length;i++){
			document.querySelector(".pantalla").value+=arrayOperaciones[i];
		}
	// console.log(operacion);
		console.log(arrayOperaciones);
	}
}

function borrar(){
	var num1=document.querySelector(".pantalla2").value;
	num1=num1.slice(0,-1);
	document.querySelector(".pantalla2").value=num1;
	console.log(num1);
}

function borrarTodo(){
	document.querySelector(".pantalla").value="";
	document.querySelector(".pantalla2").value=0;
	arrayOperaciones=[];
	console.log(arrayOperaciones);
}

function cambiar(){
	pantalla2=document.querySelector(".pantalla2").value;
	if(pantalla2.indexOf("-")==-1){
		document.querySelector(".pantalla2").value="-"+pantalla2;
	}else{
		document.querySelector(".pantalla2").value=pantalla2.slice(1);
	}
}

function clickIgual(){
	var num = parseInt(document.querySelector(".pantalla2").value);
	console.log(num);
	arrayOperaciones.push(num);
	console.log("array final:"+arrayOperaciones);
	document.querySelector(".pantalla").value="";
	console.log("longitud arrayOperaciones:"+arrayOperaciones.length);
	console.log(arrayOperaciones);
	var total=arrayOperaciones[0];
	for(var i=0;i<arrayOperaciones.length;i++){
		//if(isNaN(arrayOperaciones[i])){
			if(arrayOperaciones[i]=="+"){
				total=total+arrayOperaciones[i+1];
				console.log(total);
			}
			if(arrayOperaciones[i]=="-"){
				total=total-arrayOperaciones[i+1];
				console.log(total);
			}
			if(arrayOperaciones[i]=="/"){
				total=total/arrayOperaciones[i+1];
				console.log(total);
			}
			if(arrayOperaciones[i]=="*"){
				total=total*arrayOperaciones[i+1];
				console.log(total);
			}
		}
	//}
	document.querySelector(".pantalla").value="Total: "+total;
	document.querySelector(".pantalla2").value=0;
	arrayOperaciones=[];
}